---
layout: page
title: UTGB - Genome Browser Home
tagline: Genome Sciences at University of Tokyo
---
{% include JB/setup %}

    
## News

<ul class="posts">
  {% for post in site.posts %}
    <li><span>{{ post.date | date_to_string }}</span> &raquo; <a href="{{ BASE_PATH }}{{ post.url }}">{{ post.title }}</a></li>
  {% endfor %}
</ul>

## Genome Browsers

 &nbsp;  | &nbsp;
:---:|----
 |WormTSS <http://wormtss.utgenome.org>
[![UTGB Medaka]({{BASE_PATH}}/images/utgb-medaka-logo.gif)](http://utgenome.org/UTGBMedaka)|UTGB Medaka 
[![Machibase]({{BASE_PATH}}/images/machibase_logo.png)](http://machibase.gi.k.u-tokyo.ac.jp)|Machibase (Drosophila) 
 |UTGB Yesat <http://yeast.utgenome.org/>
 |RepeatScape <http://repeatscape.utgenome.org>







