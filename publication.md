---
layout: page
title: Publications
---
{% include JB/setup %}

* [The Transcription Start Site Landscape of C. elegans](http://genome.cshlp.org/content/23/8/1348.long). Taro L. Saito, Shin-ichi Hashimoto, Sam Guoping Gu, J. Jason Morton, Michael Stadler, Thomas Blumenthal, Andrew Fire(\*), and Shinichi Morishita(\*).  _Genome Research 2013 Aug;23(8):1348-61_. \*: joint corresponding authors 
  * WormTSS web site: <http://wormtss.utgenome.org/>

* [UTGB Toolkit for Personalized Genome Browsers](http://bioinformatics.oxfordjournals.org/cgi/content/abstract/btp350?ijkey=Ug1qLpoye8K4uEx&keytype=ref). Taro L. Saito, Jun Yoshimura, Shin Sasaki, Budrul Ahsan, Atsushi Sasaki, Reginaldo Kuroshu and Shinichi Morishita. _Bioinformatics_ (June 2009). 

* [Chromatin-Associated Periodicity in Genetic Variation Downstream of Transcriptional Start Sites](http://www.sciencemag.org/cgi/content/abstract/1163183?ijkey=2Ngfs8NYRFc9M&keytype=ref&siteid=sci). Shin Sasaki, Cecilia C. Mello, Atsuko Shimada, Yoichiro Nakatani, Shin-ichi Hashimoto, Masako Ogawa, Kouji Matsushima, Sam Guoping Gu, Masahiro Kasahara, Budrul Ahsan, Atsushi Sasaki, Taro L. Saito, Yutaka Suzuki, Sumio Sugano, Yuji Kohara, Hiroyuki Takeda, Andrew Fire, Shinichi Morishita. _Science 16 January 2009. Vol. 323. no. 5912., pp. 401-404_

* [MachiBase: A Drosophila Melanogaster 5'-end mRNA Transcription Database](http://nar.oxfordjournals.org/cgi/content/full/gkn694v1). Budrul Ahsan (\*), Taro L. Saito (\*), Shin-ichi Hashimoto, Keigo Muramatsu, Manabu Tsuda, Atushi Sasaki, Kouji Matsushima, Toshiro Aigaki, Shinichi Morishita. _Nucleic Acids Research, 2009, Vol 32, Database Issue D49-D53_. (\* joint first authors) 


* [Relational-Style XML Query](http://doi.acm.org/10.1145/1376616.1376650). Taro L. Saito, Shinichi Morishita. In _proc. of the ACM SIGMOD, International Conference on Management of Data (SIGMOD 2008), Vancouver, June 2008_ (ACM Portal http://doi.acm.org/10.1145/1376616.1376650 ) 

* [UTGB  Medaka: Genomic Resource Database for Medaka Biology](http://nar.oxfordjournals.org/cgi/content/full/gkm765?ijkey=xBOYbtjIzAIUqHv&keytype=ref). Budrul Ahsan, Daisuke Kobayashi, Tomoyuki Yamada, Masahiro Kasahara, Shin Sasaki, Taro L. Saito, Yukinobu Nagayasu, Koichiro Doi, Yoichiro Nakatani, Wei Qu, Tomoko Jindo, Atsuko Shimada, Kiyoshi Naruse, Atsushi Toyoda, Yoko Kuroki, Asao Fujiyama, Takashi Sasaki, Atsushi Shimizu, Shuichi Asakawa, Nobuyoshi Shimizu, Shin-ichi Hashimoto, Jun Yang, Yongjun Lee, Kouji Matsushima, Sumio Sugano, Mitsuru Sakaizumi, Takanori Narita, Kazuko Ohishi, Shinobu Haga, Fumiko Ohta, Hisayo Nomoto, Keiko Nogata, Tomomi Morishita, Tomoko Endo, Tadasu Shin-I, Hiroyuki Takeda, Yuji Kohara, and Shinichi Morishita. _Nucleic Acids Research, Vol. 36, Database Issue D747-D752, 2008_.

* [The Medaka Draft Genome and Insights into Vertebrate Genome Evolution](http://www.nature.com/nature/journal/v447/n7145/full/nature05846.html) Kasahara M\*, Naruse K\*, Sasaki S\*, Nakatani Y\*, Qu W, Ahsan B, Yamada T, Nagayasu Y, Doi K, Kasai Y, Jindo T, Kobayashi D, Shimada A, Toyoda A, Kuroki Y, Fujiyama A, Sasaki T, Shimizu A, Asakawa S, Shimizu N, Hashimoto S, Yang J, Lee Y, Matsushima K, Sugano S, Sakaizumi M, Narita T, Ohishi K, Haga S, Ohta F, Nomoto H, Nogata K, Morishita T, Endo T, Shin-I T, Takeda H#, Morishita S#, Kohara Y#. _Nature, 446:714-719 (2007)_ \*equal contribution, \#corresponding authors


