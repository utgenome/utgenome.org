---
layout: page
title: Download
tagline: 
---
{% include JB/setup %}

    
## Data Resources

Data resources of utgenome.org is available from <http://download.utgenome.org/pub/>.

* UTGB Meada Resources: <http://download.utgenome.org/pub/readme.html>
* Machibase Resources: <http://download.utgenome.org/pub/machibase/>

## Software

### UTGB Toolkit

 